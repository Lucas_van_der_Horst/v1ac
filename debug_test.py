import ac_formula
from ac_formula import *
import numpy as np
import ac_tests


class Constant(ac_formula.Constant):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def deriv(self):  # VOORBEELD
        """ Beschikbare instance-variablen:
            - value (getal)
            """
        return Constant(0)


class Variable(ac_formula.Variable):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def deriv(self):  # TODO
        """ Beschikbare instance-variablen:
            - label (string)
            """
        # print(f"variable deriv label: {self.label}")
        return Constant(1)


class Sum(ac_formula.Sum):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def deriv(self):  # VOORBEELD
        """ Beschikbare instance-variablen:
            - left (subformule)
            - right (subformule)
            """
        return Sum(self.left.deriv(), self.right.deriv()).simplify()


class Product(ac_formula.Product):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def deriv(self):  # TODO
        """ Beschikbare instance-variablen:
            - left (subformule)
            - right (subformule)
            """
        # print(f"product deriv: left: {self.left}, right: {self.right}")
        return Sum(
            Product(self.left.deriv(), self.right),
            Product(self.left, self.right.deriv()),
        ).simplify()


class Power(ac_formula.Power):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def deriv(self):  # TODO
        """ Beschikbare instance-variablen:
            - base (subformule)
            - exponent (getal)
            """

        # import pdb; pdb.set_trace()
        # print(f"power deriv: base: {self.base}, exponent: {self.exponent}")
        result = Product(
            Constant(self.exponent),
            Power(
                self.base,
                Sum(self.exponent, Constant(-1))
            )
        ).simplify()
        print(result)
        return result


if __name__ == '__main__':
    form = Function('f', Power(Variable('x'), -1))
    deriv = Function('f', Negative(Power(Variable('x'), -2)), 1)
    np.testing.assert_equal(form.deriv(), deriv, ac_tests.deriv_message(form, deriv))
